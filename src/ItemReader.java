import java.io.File;
import java.util.ArrayList;
import java.util.Scanner;

public class ItemReader {
    ArrayList<int[]> items = new ArrayList<>();
    private int capacity = 0;

    public ItemReader(String file) throws Exception {
        readFile(file);
    }

    private void readFile(String file) throws Exception {
        Scanner scanner = new Scanner(new File(file));

        if (scanner.hasNextLine()) {
            capacity = Integer.parseInt(scanner.nextLine());
        } else {
            throw new Exception("Incorrect input file given!");
        }
        while (scanner.hasNextLine()) {
            String nextUnsplited = scanner.nextLine();
            String[] next = nextUnsplited.split(" ");
            items.add(new int[]{Integer.parseInt(next[0]), Integer.parseInt(next[1])});
        }
    }

    public int getCapacity() {
        return capacity;
    }
}
