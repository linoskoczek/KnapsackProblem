import java.util.ArrayList;
import java.util.stream.IntStream;

public class Main {

    private static int getBit(int num, int n) {
        return (num >> n) & 1;
    }

    private static synchronized void setMax(int value, int number) {
        maxValue = value;
        maxInt = number;
    }

    private static synchronized int getMaxValue() {
        return maxValue;
    }

    private static int getTotalWeight(int p) {
        int weight = 0;
        for (int i = 0; i < numberLength; i++) {
            if (getBit(p, i) == 1)
                weight += items.get(numberLength - i - 1)[1];
        }
        return weight;
    }

    private static int getTotalValue(int p) {
        int value = 0;
        for (int i = 0; i < numberLength; i++) {
            if (getBit(p, i) == 1)
                value += items.get(numberLength - i - 1)[0];
        }
        return value;
    }

    private static ItemReader itemReader;

    static {
        try {
            itemReader = new ItemReader("15small");
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private static long startTime = System.currentTimeMillis();
    private static ArrayList<int[]> items = itemReader.items;
    private static int capacity = itemReader.getCapacity();
    private static int numberLength = items.size();
    private static int possibilities = (int) Math.pow(2, numberLength);
    private static volatile int maxValue = 0;
    private static volatile int maxInt = 0;

    public static void main(String[] args) {
        IntStream
                .range(1, possibilities)
                .parallel()
                .forEach(p -> {
                    if (getTotalWeight(p) < capacity) {
                        int value = getTotalValue(p);
                        if (value > getMaxValue()) {
                            setMax(value, p);
                        }
                    }
                });

        System.out.println("Max value is " +
                maxValue +
                " for " +
                Integer.toBinaryString(maxInt) +
                "\n" +
                (System.currentTimeMillis() - startTime) / 1000 +
                " seconds to calculate it."
        );
    }
}
